<?php 
	class DBControl {
		private $settings;
		
		static private $db;
		static private $error;
		
		private $statement;
		
		private function __construct() {
			//Carregando as propriedades do rquivo de configuração
			$this->settings = parse_ini_file("DBProp.ini");
			
			$dsn = 'mysql:host=' .$this->settings["host"] .';dbname=' .$this->settings["dbname"].';chrset=utf8';
			
			try {
				self::$db = new PDO($dsn, $this->settings["user"], $this->settings["password"]);
			}
			catch (PDOException $e) {
				self::$error = $e->getMessage();
				die('<h1>Sorry. The Database connection is temporarily unavailable.</h1>');
			}
		}
		
		static public function getConnection() {
		
			if (!self::$db) { // No PDO exists yet, so make one and send it back.
				new DBControl();
			}
		
			return self::$db;
		
		} // end function getConnection
	}
?>