CREATE TABLE user (
	id_user INT NOT NULL auto_increment, 
    nm_user VARCHAR(50) NOT NULL, 
    ds_login VARCHAR(50) NOT NULL,
    ds_password VARCHAR(50) NOT NULL, 
    ds_email VARCHAR(50) NOT NULL, 
    PRIMARY KEY (id_user), 
    UNIQUE KEY (ds_login)
);