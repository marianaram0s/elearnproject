<?php 
	class User {
		private $idUser;
		private $nmUser;
		private $dsLogin;
		private $dsPassword;
		private $dsEmail;
		
		public function __set($atrib, $valor){ //set generico
			if(property_exists($this,$atrib)){
				$this->$atrib = $valor;
			} else{
				echo 'O atributo ' . $atrib . ' n�o existe';
			}
		}
		
		public function __get($atrib){ //get generico
			if(property_exists($this,$atrib)){
				return $this->$atrib;
			} else{
				echo 'O atributo '.$atrib.' n�o existe';
			}
		}
		
	}
?>